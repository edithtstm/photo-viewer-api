const { Router } = require('express');
const addWaterMark = require('../utils/images');
const aws = require('aws-sdk');
const router = Router();
const Jimp = require("jimp");

// Set S3 endpoint to DigitalOcean Spaces
const spacesEndpoint = new aws.Endpoint('sfo3.digitaloceanspaces.com');
let space = new aws.S3({
    endpoint: spacesEndpoint,
    useAccelerateEndpoint: false,
  });
  
//Name of your bucket here
const BucketName = "vivaspace";

router.post('/images', (req, res) => {    
    addWaterMark(req.body.data).then(image => {
        image.getBufferAsync(Jimp.AUTO).then((file) =>{
            const currentDate = new Date();
            const timestamp = currentDate. getTime();
            let uploadParameters = {
                Bucket: BucketName,
                ContentType: "image/jpeg",
                Body: file,
                ACL: 'public-read',
                Key: `viva-advance-${timestamp}.jpeg`
            };

            space.upload(uploadParameters, function (error, data) {
                if (error){
                    console.error(error);
                    res.sendStatus(500);
                } else {
                    res.sendStatus(201);
                }
            });
        });
    });
})

router.get('/images', (req, res) => {
    let listParameters = {
        Bucket: BucketName,
    };
    space.listObjectsV2(listParameters, function (error, data) {
        if (error){
            console.error(error);
            res.sendStatus(500);
        } else {
            data.Contents.reverse();
            const response = data.Contents.map((file) => {
            return {  
                original: `https://${BucketName}.sfo3.digitaloceanspaces.com/${file.Key}`
            }});
            res.json(
                {
                    "data": response,
                    "total": data.KeyCount
                }
            );
        }
    })
})

router.get('/images/:key', (req, res) => {
    console.log('Request Id:', req.params.key);
    const url = space.getSignedUrl('getObject', {
        Bucket: BucketName,
        Key: req.params.key,
        Expires: 3000,
    })
    console.log(url)
    res.json(
        {
            "url": url
        }
    );
})
 
module.exports = router;